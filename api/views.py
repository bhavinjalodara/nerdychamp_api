# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from api.ViewController.InterestViewSet import InterestViewSet
from api.ViewController.UserViewSet import UserViewSet
from api.ViewController.ProfileViewSet import ProfileViewSet
from api.ViewController.VoteViewSet import VoteViewSet
from api.ViewController.PostViewSet import PostViewSet
from api.ViewController.CommentViewSet import CommentViewSet
from api.ViewController.FileViewSet import FileViewSet
from api.ViewController.InceptViewSet import InceptViewSet

# Create your views here.
