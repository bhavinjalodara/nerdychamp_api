from time import strftime

from api.enum.Action import Action

from api.submodels.UserDetails import UserDetails

from api.submodels.Interest import Interest

from django.db.models import fields

from api.enum.PostStatus import PostStatus

from api.submodels.Vote import Vote

from api.enum.VoteType import VoteType

from api.Serializers.VoteSerializer import VoteSerializer

from api.ChampUtil.req import dataExtractor, Req, paginate, Order

from api.data.weights import PostValue

from api.submodels.PostWeight import PostWeight

from api.submodels.Activities import Activities

from api.enum.ActivityType import ActivityType
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from api.Serializers.PostSerializer import PostSerializer

from api.submodels.PostDetails import PostDetails
from django.db.models import Q,Count,OuterRef, Subquery

class PostViewSet(viewsets.ViewSet):
    
    postQuerySet = PostDetails.objects.all()

    @list_route(methods=['POST'])
    def createPost(self, request):

        data = dataExtractor(request, Req.POST, ('userId', 'actionType'))
        if type(data) is Response:
            return data

        postId = data.get('postId')
        postText = data.get('postText')
        fileURL = data.get('fileURL')
        interests = data.get('interests')
        userId = data.get('userId')
        actionType = data.get('actionType')
        if actionType is not None and userId is not None:
            user=UserDetails.objects.filter(id=userId).first()
            if(interests  is None):
                interests=[]
            if actionType==Action.DELETE or actionType==Action.UPDATE or actionType==Action.UPDATEDRAFT or len(interests)==0:
                if postId is None:
                    response = {
                        "success": False,
                        "message": "post Id is invalid",
                        "postId": postId,
                        "interests":interests,
                        "actionType":actionType
                    }
                    return Response(response)
                else:
                    post=self.postQuerySet.filter(id=postId).first()
                    if post.user!=user:
                        response = {
                            "success": False,
                            "message": "error access denied",
                            "userId": userId
                        }
                        return Response(response)
                    if post is None:
                        response = {
                            "success": False,
                            "message": "error retriving post",
                            "postId": postId
                        }
                        return Response(response)
            else:
                post=PostDetails()
                post.user=user
                post.save()
                weight = PostWeight(post_id=post.id, reminder=PostValue.CREATE)
                weight.save()
            if postText != None:
                post.postText = postText
            if fileURL != None:
                post.fileURL = fileURL
            if data.get('isUrlLink') != None:
                post.isUrlLink = data.get('isUrlLink')
            if data.get('urlTitle') != None:
                post.urlTitle = data.get('urlTitle')
            if data.get('urlDescription') != None:
                post.urlDescription = data.get('urlDescription')
            if data.get('urlImage') != None:
                post.urlImage = data.get('urlImage')
            if data.get('urlLink') != None:
                post.urlLink = data.get('urlLink')
            if data.get('extraInterests') != None:
                for interest in data.get('extraInterests'):
                    try:
                        interests.append(Interest.objects.create(interestName=interest).id)
                    except:
                        pass;
            if actionType==Action.NEW:
                post.status=PostStatus.APPROVED
                post.createdTime = strftime("%Y-%m-%d %H:%M:%S")
                post.modifiedTime = strftime("%Y-%m-%d %H:%M:%S")
                post.save()
                activity = Activities(user_id=userId,post_id=post.id,action=ActivityType.POST)
                activity.createdTime = strftime("%Y-%m-%d %H:%M:%S")
                activity.save()
            elif actionType==Action.UPDATE:
                post.status = PostStatus.APPROVED
                post.modifiedTime = strftime("%Y-%m-%d %H:%M:%S")
            elif actionType == Action.DELETE:
                post.status = PostStatus.DELETED
                post.deletedTime = strftime("%Y-%m-%d %H:%M:%S")
            elif actionType == Action.DRAFT:
                post.status = PostStatus.DRAFT
            elif actionType == Action.UPDATEDRAFT:
                post.status = PostStatus.DRAFT
            post.ipAddress = request.META.get('REMOTE_ADDR')
            post.agent = request.META.get('HTTP_USER_AGENT').split(" ")[0]
            post.save()
            if len(interests) > 0:
                post.interests.set(interests)

            response = {
                "data": PostSerializer(post).data,
                "success": True
            }
            return Response(response)
        else:
            response = {
                "success": False,
                "message": "profile Id or Post text or type or interest list is null "
            }
            return Response(response)


    @list_route(methods=['POST'])
    def getPost(self, request):
        data = dataExtractor(request, Req.POST, ('postId'))
        if type(data) is Response:
            return data

        posts=self.postQuerySet.filter(id=data['postId']).first()
        if posts is None:
            response = {
                "success": False,
                "message": "post doesnt exists for given id",
                "postId": data['postId']
            }
            return Response(response)
        return Response(PostSerializer(posts).data)

    @list_route(methods=['GET','POST'])
    def getPostsByUser(self, request):
        if request.method=='GET':
            GET = True
        else:
            GET = None
        data = dataExtractor(request, Req.POST, ('userId', 'profileId'),GET)
        if type(data) is Response:
            return data

        user = UserDetails.objects.filter(id=data['profileId']).first()
        if user is None:
            response = {
                "success": False,
                "message": "post doesnt exists for given id",
                "userId": data['userId']
            }
            return Response(response)
        posts=paginate(user.userPosts.filter(status=PostStatus.APPROVED), request, Order.ID)
        posts=PostSerializer(posts, many=True).data
        for post in posts:
            vote = Vote.objects.filter(post_id=post['id'], user_id=data['userId']).first()
            if vote is None:
                post['vote']=None
                post['voteCount']=None
            else:
                post['vote'] = VoteSerializer(vote).data
                post['voteCount'] = Vote.objects.filter(post_id=post['id'], type=VoteType.UPVOTE).count()
        response = {
            "data": posts,
            "success": True
        }
        return Response(response)

    @list_route(methods=['GET', 'POST'])
    def getPostsByHome(self, request):
        if request.method == 'GET':
            GET = True
        else:
            GET = None
        data = dataExtractor(request, Req.POST, ('userId'), GET)
        if type(data) is Response:
            return data
        interests=list(request._user.interests.all())
        followings=list(request._user.followings.all())

        posts = paginate(PostDetails.objects.filter(
            Q(interests__in=interests) | Q(user__in=followings),
            status=PostStatus.APPROVED
        ),request , Order.ID)

        # posts = paginate(PostDetails.objects.filter(
        #     Q(interests__in=interests) | Q(user__in=followings)
        # ).order_by('-weight__qoutient', '-weight__reminder'), request, order=False)

        posts = PostSerializer(posts, many=True).data
        for post in posts:
            vote = Vote.objects.filter(post_id=post['id'], user_id=data['userId']).first()
            if vote is None:
                post['vote'] = None
                post['voteCount'] = None
            else:
                post['vote'] = VoteSerializer(vote).data
                post['voteCount'] = Vote.objects.filter(post_id=post['id'], type=VoteType.UPVOTE).count()
        response = {
            "data": posts,
            "success": True
        }
        return Response(response)



    @list_route(methods=['GET','POST'])
    def getPostsByInterest(self, request):
        if request.method == 'GET':
            GET = True
        else:
            GET = None
        data = dataExtractor(request, Req.POST, ('userId', 'interestId'), GET)
        if type(data) is Response:
            return data

        interest= Interest.objects.filter(id=data['interestId']).first()
        if interest is None:
            response = {
                "success": False,
                "message": "post doesnt exists for given id",
                "interestId": data['interestId']
            }
            return Response(response)
        vo=Vote.objects.filter(post_id=OuterRef('id'), type=VoteType.UPVOTE).values('id').annotate(count=Count('id')).values('count')
        vo.query.group_by = []

        # PostDetails.objects.filter(interests__in=[data['interestId']], status=PostStatus.APPROVED).order_by('-weight__qoutient', '-weight__reminder')

        posts=PostDetails.objects.filter(interests__in=[data['interestId']],status=PostStatus.APPROVED).annotate(voteCount=Subquery(vo,output_field=fields.IntegerField())).order_by('-voteCount')
        posts = paginate(posts , request, order=False)
        posts = PostSerializer(posts, many=True).data
        for post in posts:
            vote = Vote.objects.filter(post_id=post['id'], user_id=data['userId']).first()
            if vote is None:
                post['vote'] = None
                post['voteCount'] = None
            else:
                post['vote'] = VoteSerializer(vote).data
                post['voteCount'] = Vote.objects.filter(post_id=post['id'], type=VoteType.UPVOTE).count()
        response = {
            "data": posts,
            "success": True
        }
        return Response(response)

    @list_route(methods=['GET','POST'])
    def getPostsByInterests(self, request):
        if request.method == 'GET':
            GET = True
        else:
            GET = None
        data = dataExtractor(request, Req.POST, ('userId', 'interests'), GET)
        if type(data) is Response:
            return data

        posts = self.postQuerySet.filter(interests__in=data['interests'], status=PostStatus.APPROVED).distinct()
        if posts is None:
            response = {
                "success": False,
                "message": "post doesnt exists for given id",
                "interestId": data['interests']
            }
            return Response(response)
        posts = PostSerializer(posts, many=True).data
        for post in posts:
            vote = Vote.objects.filter(post_id=post['id'], user_id=data['userId']).first()
            if vote is None:
                post['vote'] = None
                post['voteCount'] = None
            else:
                post['vote'] = VoteSerializer(vote).data
                post['voteCount'] = Vote.objects.filter(post_id=post['id'], type=VoteType.UPVOTE).count()
        response = {
            "data": posts,
            "success": True
        }
        return Response(response)


    @list_route(methods=['POST'])
    def deletePost(self, request):
        data = dataExtractor(request, Req.POST, ('postId'))
        if type(data) is Response:
            return data

        posts = self.postQuerySet.filter(id=data['postId']).first()
        if posts is None:
            response = {
                "success": False,
                "message": "post doesnt exists for given id",
                "postId": data['postId']
            }
            return Response(response)
        posts.delete()
        response = {
            "success": True,
            "message": "post deleted from databases",
            "postId": data['postId']
        }
        return Response(response)