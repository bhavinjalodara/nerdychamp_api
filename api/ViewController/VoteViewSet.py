from time import strftime

from api.ChampUtil.req import dataExtractor, Req

from api.enum.VoteType import VoteType

from api.submodels.PostWeight import PostWeight

from api.data.weights import PostValue

from api.submodels.Activities import Activities

from api.enum.ActivityType import ActivityType
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from api.Serializers.VoteSerializer import VoteSerializer

from api.submodels.Vote import Vote

class VoteViewSet(viewsets.ViewSet):

    voteQuerySet = Vote.objects.all()

    @list_route(methods=['POST'])
    def createVote(self, request):
        data = dataExtractor(request, Req.VOTE, ('postId', 'userId', 'voteType'))
        if type(data) is Response:
            return data

        postId = data['postId']
        userId = data['userId']
        voteType = data['type']
        if(voteType!=0 and voteType!=1 and voteType!=2):
            response = {
                "success": False,
                "message": "invalid vote voteType"
            }
            return Response(response)
        vote = self.voteQuerySet.filter(post_id=postId, user_id=userId).first()
        if vote is None:
            vote=Vote(post_id=postId,user_id=userId)
            vote.createdTime = strftime("%Y-%m-%d %H:%M:%S")
        vote.modifiedTime = strftime("%Y-%m-%d %H:%M:%S")
        vote.type=voteType
        vote.save()
        postweight = PostWeight.objects.filter(post_id=postId).first()
        if postweight is None:
            postweight = PostWeight(post_id=postId, reminder= PostValue.CREATE)
        if voteType == VoteType.UPVOTE:
            postweight.reminder += PostValue.UPVOTE
            activity = Activities(user_id=userId, post_id=postId, action=ActivityType.UPVOTE)
            activity.createdTime = strftime("%Y-%m-%d %H:%M:%S")
            activity.save()
        if voteType == VoteType.DOWNVOTE:
            postweight.reminder += PostValue.DOWNVOTE
        if voteType == VoteType.DEFAULT:
            postweight.reminder += PostValue.DEFAULT
        if postweight.reminder>= PostValue.LIMIT:
            postweight.qoutient += 1
            postweight.reminder %= PostValue.LIMIT
        postweight.save()
        response = {
            "success": True,
            "message": "created vote",
            "upVote": Vote.objects.filter(post_id=postId, type=VoteType.UPVOTE).count()
        }
        return Response(response)


    @list_route(methods=['POST'])
    def getVote(self, request):
        data = dataExtractor(request, Req.VOTE, ('postId'))
        if type(data) is Response:
            return data
        votes=self.voteQuerySet.filter(post_id=data['postId'])
        if votes is None:
            response = {
                "success": True,
                "message": "No votes found",
                "votes":[]
            }
            return Response(response)
        else:
            return Response(VoteSerializer(votes,many=True).data)
    #
    #
    # @list_route(methods=['POST'])
    # def upVote(self, request):
    #
    # @list_route(methods=['POST'])
    # def downVote(self, request)