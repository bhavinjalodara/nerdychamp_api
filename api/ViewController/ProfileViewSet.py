from time import strftime

from api.submodels.UserDetails import UserDetails
from api.Serializers.UserSerializer import UserSerializer

from api.Serializers.FollowSerializer import FollowSerializer

from api.submodels.Interest import Interest

from api.enum.UserStatus import UserStatus

from api.ChampUtil.req import dataExtractor, Req

from api.data.weights import UserValue

from api.submodels.UserWeight import UserWeight

from api.submodels.Activities import Activities

from api.enum.ActivityType import ActivityType
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route

class ProfileViewSet(viewsets.ViewSet):
    
    userQuerySet = UserDetails.objects.all()

    @list_route(methods=['POST'])
    def updateProfile(self,request):

        data = dataExtractor(request, Req.USER, ('email', 'userId'))
        if type(data) is Response:
            return data
        
        userId = data.get('userId')
        email=data.get('email')
        dob=data.get('dob')
        fname=data.get('fname')
        lname=data.get('lname')
        sex=data.get('sex')
        password=data.get('password')
        profilePicURL=data.get('profilePicURL')
        interests = data.get('interests')
        user = self.userQuerySet.filter(id=userId).first()
        if user is None:
            response = {
                "success": False,
                "message": "profile doesnt exists"
            }
            return Response(response)
        else:
            if(user.email!=email):
                count=self.userQuerySet.filter(email=email).first()
                if count is None:
                    user.email=email
                else:
                    response = {
                        "success": False,
                        "message": "email id already exists"
                    }
                    return Response(response)
        if dob is not None:
            user.dob=dob
        if fname is not None:
            user.fname =fname
        if lname is not None:
            user.lname =lname
        if sex is not None:
            user.sex =sex
        if password  is not None:
            user.set_password(password)
        if profilePicURL is not None:
            user.profilePicURL =profilePicURL
        if interests is not None:
            user.interests.set(Interest.objects.filter(id__in=interests))
        user.modifiedTime = strftime("%Y-%m-%d %H:%M:%S")
        user.ipAddress = request.META.get('REMOTE_ADDR')
        user.agent = request.META.get('HTTP_USER_AGENT').split(" ")[0]
        user.save()
        response={
            'data': UserSerializer(user).data,
            "success": True
        }
        return Response(response)

    @list_route(methods=['POST'])
    def getProfileByEmail(self, request):
        data = dataExtractor(request, Req.USER, ('email'))
        if type(data) is Response:
            return data


        user=self.userQuerySet.filter(email=data['email']).first()
        if user is None:
            response = {
                "success": False,
                "message": "profile Not Found"
            }
            return Response(response)
        response = {
            "data": UserSerializer(user).data,
            "success": True
        }
        return Response(response)

    @list_route(methods=['POST'])
    def getProfileById(self, request):
        data = dataExtractor(request, Req.USER, ('userId', 'profileId'))
        if type(data) is Response:
            return data

        if data['profileId'] == request._user.id:
            data = UserSerializer(request._user).data
            response = {
                "data": data,
                "success": True
            }
            return Response(response)
        user = self.userQuerySet.filter(id=data['profileId']).first()
        if user is None:
            response = {
                "success": False,
                "message": "profile Not Found"
            }
            return Response(response)
        data=UserSerializer(user).data
        if request._user.followings.filter(id=user.id).first():
            data["isFollowing"]=True
        else:
            data["isFollowing"] = False
        response={
            "data": data,
            "success": True
        }
        return Response(response)

    @list_route(methods=['POST'])
    def deactivateProfile(self, request):
        data = dataExtractor(request, Req.USER, ('userId'))
        if type(data) is Response:
            return data

        user = self.userQuerySet.filter(id=data['userId']).first()
        if user is None:
            response = {
                "success": False,
                "message": "profile Not Found"
            }
            return Response(response)
        user.status=UserStatus.INACTIVE
        user.save()
        response={
            "success":True,
        }
        return Response(response)

    @list_route(methods=['POST'])
    def follow(self,request):
        data = dataExtractor(request, Req.USER, ('userId', 'followId'))
        if type(data) is Response:
            return data

        user=self.userQuerySet.get(id=data['userId'])
        followUser=self.userQuerySet.get(id=data['followId'])
        user.followings.add(followUser)

        activity = Activities(user_id=data['userId'],targetUser_id=data['followId'],action=ActivityType.FOLLOW)
        activity.createdTime = strftime("%Y-%m-%d %H:%M:%S")
        activity.save()

        userweight = UserWeight.objects.filter(user_id=followUser.id).first()
        if userweight is None:
            userweight = UserWeight(user_id=followUser.id, reminder=UserValue.CREATE)
        userweight.reminder += UserValue.FOLLOW
        if userweight.reminder >= UserValue.LIMIT:
            userweight.qoutient += 1
            userweight.reminder %= UserValue.LIMIT
        userweight.save()
        response={
            "success":True,
        }
        return Response(response)

    @list_route(methods=['POST'])
    def unfollow(self, request):

        data = dataExtractor(request, Req.USER, ('userId', 'followId'))
        if type(data) is Response:
            return data
        user = self.userQuerySet.get(id=data['userId'])
        followUser = self.userQuerySet.get(id=data['followId'])
        user.followings.remove(followUser)

        userweight = UserWeight.objects.filter(user_id=followUser.id).first()
        if userweight is None:
            userweight = UserWeight(user_id=followUser.id, reminder=UserValue.CREATE)
        userweight.reminder += UserValue.UNFOLLOW
        if userweight.reminder >= UserValue.LIMIT:
            userweight.qoutient += 1
            userweight.reminder %= UserValue.LIMIT
        userweight.save()
        response = {
            "success": True,
        }
        return Response(response)

    @list_route(methods=['POST'])
    def getFollowing(self,request):
        data = dataExtractor(request, Req.USER, ('userId'))
        if type(data) is Response:
            return data
        user = self.userQuerySet.get(id=data['userId'])
        following=FollowSerializer(user.followings,many=True)
        return Response(following.data)

    @list_route(methods=['POST'])
    def getFollower(self, request):
        data = dataExtractor(request, Req.USER, ('userId'))
        if type(data) is Response:
            return data
        user = self.userQuerySet.get(id=data['userId'])
        follower = FollowSerializer(user.followers, many=True)
        return Response(follower.data)

    @list_route(methods=['POST'])
    def getFollowingCount(self, request):
        data = dataExtractor(request, Req.USER, ('userId'))
        if type(data) is Response:
            return data
        user = self.userQuerySet.get(id=data['userId'])
        count=user.followings.count()
        response = {
            "success": True,
            "count":count
        }
        return Response(response)

    @list_route(methods=['POST'])
    def getFollowerCount(self, request):
        data = dataExtractor(request, Req.USER, ('userId'))
        if type(data) is Response:
            return data

        user = self.userQuerySet.get(id=data['userId'])
        count = user.followers.count()
        response = {
            "success": True,
            "count": count
        }
        return Response(response)