from api.Serializers.InterestSerializer import InterestSerializer
from api.submodels.Interest import Interest
from api.submodels.UserDetails import UserDetails

from api.Serializers.UserSerializer import UserSerializer

from api.ChampUtil.req import dataExtractor, Req
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from rest_framework_jwt.settings import api_settings


class InterestViewSet(viewsets.ViewSet):

    interestQuerySet = Interest.objects.all()
    i=bool(interestQuerySet)

    @list_route()
    def allInterest(self, request):
        interests = self.interestQuerySet.filter(status="active")
        response = InterestSerializer(interests, many=True)
        return Response(response.data)

    @list_route()
    def allRootInterest(self,request):
        interests=self.interestQuerySet.filter(status='active', parentInterestId=0)
        response = InterestSerializer(interests, many=True)
        response={
            "data":response.data,
            "success":True
        }
        return Response(response)

    @list_route(methods=['POST'])
    def getInterest(self,request):
        jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
        get_user_id=api_settings.JWT_PAYLOAD_GET_USER_ID_HANDLER
        x=jwt_decode_handler(request._auth)
        print get_user_id(x)
        interestId = request.data.get('interestId',-1)
        interest = self.interestQuerySet.get(id=interestId)
        serializer = InterestSerializer(interest)
        return Response(serializer.data)

    @list_route(methods=['POST'])
    def addUserInterests(self,request):
        data = dataExtractor(request, Req.INTEREST, ('userId','interests'))
        if type(data) is Response:
            return data
        request._user.interests.set(data['interests'])
        print request._user.isInterestConfigured
        request._user.isInterestConfigured=True
        print request._user.isInterestConfigured
        request._user.save()
        response = {
            "interests": data['interests'],
            "message": "request added successfully",
            "success": True
        }
        return Response(response)

    @list_route(methods=['POST'])
    def getChildInterests(self, request):
        data = dataExtractor(request, Req.INTEREST, ('parentInterestId'))
        if type(data) is Response:
            return data

        interests = self.interestQuerySet.filter(parentInterestId=data['parentInterestId'])
        serializer = InterestSerializer(interests,many=True)
        return Response(serializer.data)
