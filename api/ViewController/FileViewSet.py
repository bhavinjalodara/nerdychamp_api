from rest_framework import viewsets
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route

from api.Serializers.FileSerializer import FileSerializer

from api.submodels.FileDetails import FileDetails
from api.submodels.UserDetails import UserDetails
import os
from time import strftime
import uuid


class FileViewSet(viewsets.ViewSet):
    # users=UserDetails.objects.all()

    @list_route(methods=['POST'],parser_classes=(JSONParser, MultiPartParser,))
    def upload(self, request):

        userId = request.data.get('userId')

        # user=self.users.filter(id=userId).first()
        if userId is None:
            response={
                "success":False,
                "message":"user id required"
            }

        file = request.FILES['file']
        unique_id = uuid.uuid4().hex[:17]
        name = str(userId)+"_"+"%Y%m%d" +"_"+unique_id

        fileExtention = os.path.splitext(file.name)[1]
        filename=strftime(name)+fileExtention
        fileURL = "/var/www/html/assets/uploaded/"+filename
        outputFile = open(fileURL, 'w+')
        for chunk in file.chunks():
            outputFile.write(chunk)
        outputFile.close()
        fileURL=outputFile.name
        entry = FileDetails(user=request._user, fileURL=fileURL)
        entry.save()
        data = "/uploaded/"+filename
        print "data="+data
        print "filename="+filename
        response={
            "success": True,
            "message": "file Uploaded SuccessFully",
            "data": data
        }
        return Response(response)