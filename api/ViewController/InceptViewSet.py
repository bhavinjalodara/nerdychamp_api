from api.ChampUtil.req import dataExtractor, Req
from api.Serializers.FollowSerializer import FollowSerializer
from api.Serializers.InterestSerializer import InterestSerializer
from api.submodels.Interest import Interest
from api.submodels.UserDetails import UserDetails

from bs4 import BeautifulSoup
from django.db.models import Q
from api.submodels.Activities import Activities

from api.Serializers.ActivitySerializer import ActivitySerializer

from pip._vendor import requests
from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response


class InceptViewSet(viewsets.ViewSet):

    @list_route(methods=['POST'])
    def search(self,request):
        data = dataExtractor(request, Req.SEARCH, ('userId', 'query'))
        if type(data) is Response:
            return data

        users=UserDetails.objects.filter(
            Q(fname__istartswith=data['query']) | Q(lname__istartswith=data['query'])
        )[0:5]
        users=FollowSerializer(users, many=True).data

        interests=Interest.objects.filter(interestName__istartswith=data['query'])[0:5]
        interests=InterestSerializer(interests, many=True).data
        response={
            'data':{
                'users':    users,
                'interests':    interests
            },
            'success':  True
        }
        return Response(response)

    @list_route(methods=['POST'])
    def activities(self, request):
        data = dataExtractor(request, Req.SEARCH, ('userId'))
        if type(data) is Response:
            return data

        activity=Activities.objects.filter(user__in=request._user.followings.all())[0:20]
        activities = ActivitySerializer(activity, many=True).data

        response = {
            'data': activities,
            'success': True
        }
        return Response(response)

    @list_route(methods=['POST'])
    def fetchUrl(self, request):
        data = dataExtractor(request, Req.SEARCH, ('userId', 'url'))
        if type(data) is Response:
            return data
        urlData = data.get('url')
        if urlData.find("http")!=0:
            url="http://"+urlData
        else:
            url=urlData
        try:
            res=requests.get(url)
        except:
            response={
                'data': {
                    'url': urlData,
                    'images': [],
                    'description': None,
                    'title': urlData,
                },
                'success':  True
            }
            return Response(response)
        if res.status_code != requests.codes.ok:
            response = {
                'data': {
                    'url': urlData,
                    'images': [],
                    'description': None,
                    'title': urlData,
                },
                'success': True
            }
            return Response(response)
        html=BeautifulSoup(res.text,'html.parser')
        title=html.title.string

        metas=html.find_all("meta")

        description=None
        image = None
        for meta in metas:
            if meta.get("name")=='description':
                description=meta.get('content')
            if meta.get('property') == 'og:image':
                image = {
                    'name': "OpenGraph Image",
                    'src': meta.get('content')
                }


        img=html.find_all('img')
        img=img[0:10]
        imgs=[]
        for elm in img:
            name=elm.get('name')
            src=elm.get('src')
            imgs.append(
                {
                    'name': name,
                    'src':  src
                }
            )
        if image!=None:
            imgs=[image]+imgs
        print imgs
        response={
            'data': {
                'url': urlData,
                'images': imgs,
                'description': description,
                'title': title,
            },
            'success':  True
        }
        return Response(response)



