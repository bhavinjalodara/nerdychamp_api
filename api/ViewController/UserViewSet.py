from time import strftime

from api.Serializers.UserSerializer import UserSerializer
from api.submodels.UserDetails import UserDetails

from api.data.weights import UserValue

from api.submodels.UserWeight import UserWeight
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from rest_framework_jwt.settings import api_settings
from api.ChampUtil.req import dataExtractor, Req

class UserViewSet(viewsets.ViewSet):
    
    userQuerySet = UserDetails.objects.all()

    permission_classes = []
    @list_route(methods=['POST'])
    def signup(self,request):
        data = dataExtractor(request, Req.SIGNUP)
        if type(data) is Response:
            return data
        try:
            user = self.userQuerySet.get(email=data['email'])
            response = {'message':'email already exists','success':False}
            return Response(response)
        except:
            user=UserDetails(email=data['email'], fname=data['fname'], lname=data['lname'])
            user.set_password(data['password'])
            user.isInterestConfigured=False
            user.createdTime = strftime("%Y-%m-%d %H:%M:%S")
            user.modifiedTime = strftime("%Y-%m-%d %H:%M:%S")
            user.ipAddress = request.META.get('REMOTE_ADDR')
            user.agent = request.META.get('HTTP_USER_AGENT').split(" ")[0]
            user.save()
            weight = UserWeight(user_id=user.id, reminder=UserValue.CREATE)
            weight.save()
            return self.login(request)

    @list_route(methods=['POST'])
    def login(self, request):
        data = dataExtractor(request, Req.LOGIN)
        if type(data) is Response:
            return data

        try:
            user = self.userQuerySet.get(email=data['email'])
            if user.check_password(data['password']):

                jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
                payload = jwt_payload_handler(user)
                token = jwt_encode_handler(payload)
                user = UserSerializer(user).data
                user['token'] = token
                response = {
                    "data": user,
                    "success": True
                }
                print response
                return Response(response)
            else:
                response = {'message': 'password didnt match', 'success': False }
                return Response(response)
        except Exception:
            response = {'message': 'email doesn\'t exists', 'success': False}
            return Response(response)
