from time import strftime
from api.enum.Action import Action

from api.submodels.UserDetails import UserDetails

from api.submodels.Interest import Interest

from api.enum.CommentStatus import CommentStatus

from api.ChampUtil.req import Req, dataExtractor

from api.submodels.Activities import Activities

from api.enum.ActivityType import ActivityType
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from api.Serializers.CommentSerializer import CommentSerializer

from api.submodels.CommentDetails import CommentDetails

class CommentViewSet(viewsets.ViewSet):

    commentQuerySet = CommentDetails.objects.all()
    c=bool(commentQuerySet)

    @list_route(methods=['POST'])
    def createComment(self, request):

        data = dataExtractor(request, Req.COMMENT, ('userId', 'postId', 'commentText'))
        if type(data) is Response:
            return data

        commentId = data.get('commentId')
        commentText = data.get('commentText')
        userId = data.get('userId')
        postId = data.get('postId')
        actionType = data.get('actionType')

        if actionType is not None and userId is not None and commentText is not None and postId is not None:
            user=UserDetails.objects.filter(id=userId).first()
            # post=self.postObjects.filter(id=postId).first()
            if actionType==Action.DELETE or actionType==Action.UPDATE or actionType==Action.UPDATEDRAFT:
                if commentId is None:
                    response = {
                        "success": False,
                        "message": "comment Id is invalid",
                        "commentId": commentId,
                        "actionType":actionType
                    }
                    return Response(response)
                else:
                    comment=self.commentQuerySet.filter(id=commentId).first()
                    if comment is None:
                        response = {
                            "success": False,
                            "message": "error retriving comment",
                            "commentId": commentId
                        }
                        return Response(response)
            else:
                comment=CommentDetails()
                comment.user=user
                comment.post_id=postId
            comment.commentText=commentText
            if actionType==Action.NEW:
                comment.commentStatus=CommentStatus.APPROVED
                comment.createdTime = strftime("%Y-%m-%d %H:%M:%S")
                comment.modifiedTime = strftime("%Y-%m-%d %H:%M:%S")
                activity = Activities(user_id=userId, post_id=postId, action=ActivityType.POST)
                activity.createdTime = strftime("%Y-%m-%d %H:%M:%S")
                activity.save()
            elif actionType==Action.UPDATE:
                comment.commentStatus = CommentStatus.APPROVED
                comment.modifiedTime = strftime("%Y-%m-%d %H:%M:%S")
            elif actionType == Action.DELETE:
                comment.commentStatus = CommentStatus.DELETED
                comment.deletedTime = strftime("%Y-%m-%d %H:%M:%S")
            elif actionType == Action.DRAFT:
                comment.commentStatus = CommentStatus.DRAFT
            elif actionType == Action.UPDATEDRAFT:
                comment.commentStatus = CommentStatus.DRAFT
            comment.ipAddress = request.META.get('REMOTE_ADDR')
            comment.agent = request.META.get('HTTP_USER_AGENT').split(" ")[0]
            comment.save()
            return Response(CommentSerializer(comment).data)
        else:
            response = {
                "success": False,
                "message": "profile Id or Comment text or type or post id is null "
            }
            return Response(response)


    @list_route(methods=['POST'])
    def getComment(self, request):
        data = dataExtractor(request, Req.COMMENT, ('commentId'))
        if type(data) is Response:
            return data

        comments=self.commentQuerySet.filter(id=data['commentId']).first()
        if comments is None:
            response = {
                "success": False,
                "message": "comment doesnt exists for given id",
                "commentId": data['commentId']
            }
            return Response(response)
        return Response(CommentSerializer(comments).data)

    @list_route(methods=['POST'])
    def getCommentsByPost(self, request):

        data = dataExtractor(request, Req.COMMENT, ('postId'))
        if type(data) is Response:
            return data

        comments = self.commentQuerySet.filter(post_id=data['postId'], status=CommentStatus.APPROVED)
        if comments is None:
            response = {
                "success": False,
                "message": "comment doesnt exists for given id",
                "postId": data['postId']
            }
            return Response(response)
        return Response(CommentSerializer(comments,many=True).data)

    @list_route(methods=['POST'])
    def getCommentsCount(self, request):
        data = dataExtractor(request, Req.COMMENT, ('postId'))
        if type(data) is Response:
            return data

        count = self.commentQuerySet.filter(post_id=data['postId'], status=CommentStatus.APPROVED).count()
        response = {
            "success": True,
            "message": "comments count retrieved",
            "postId": data['postId'],
            "count" : count
        }
        return Response(response)