from rest_framework.response import Response
from collections import namedtuple


__req = namedtuple('RequestType', ['SIGNUP', 'LOGIN', 'COMMENT', 'INTEREST', 'POST', 'USER', 'VOTE', 'SEARCH'])
Req = __req('signup', 'login', 'comment', 'interest', 'post', 'user', 'vote', 'search')

__order = namedtuple('OrderBy', ['VOTE', 'ID'])
Order = __order('vote', 'id')

requestMapping = {
    'signup': {
        'fields': ('fname', 'lname', 'email', 'password'),
        'userCheckSkip': True,

    },
    'login': {
        'fields': ('email', 'password'),
        'userCheckSkip': True
    },
    'comment': {
        'fields': ('commentId', 'userId', 'postId', 'commentId', 'commentText', 'actionType')
    },
    'interest': {
        'fields': ('interests', 'userId', 'parentInterestId')
    },
    'post': {
        'fields': ('postId', 'userId', 'interestId', 'interests', 'actionType', 'fileURL','extraInterests', 'postText', 'profileId', 'isUrlLink', 'urlTitle', 'urlDescription', 'urlImage', 'urlLink')
    },
    'user': {
        'fields': ('followId', 'userId', 'email', 'dob', 'fname', 'lname', 'sex', 'password', 'profilePicURL', 'interests', 'profileId')
    },
    'vote': {
        'fields': ('postId', 'userId', 'type')
    },
    'search':{
        'fields': ('userId', 'query', 'url')
    }
}

orderByMapping = {
    'vote':{
        'reverse': True,
    },
    'id':{
        'reverse': True,
    }
}

def dataExtractor(request, type, required = None,GET = None):
    data = {}
    success = True
    error = []
    userSkipCheck = requestMapping[type].get('userCheckSkip')
    if not userSkipCheck:
        if not GET:
            userId = request.data.get('userId')
        else:
            userId = request.query_params.get('userId')
        if userId is None:
            response={
                "success": False,
                "message": "userId is not provided",
                "required": ['userId']
            }
            return Response(response)
        if int(userId) != request._user.id:
            response={
                "success": False,
                "message": "Authentication failed user mismatch",
                "required": ['userId']
            }
            return Response(response, status=401)
    fields = requestMapping[type]['fields']
    if required is None:
        required = fields
    for field in fields:
        if not GET:
            data[field] = request.data.get(field)
        else:
            data[field] = request.query_params.get(field)

        if field in required and data[field] is None:
            success = False
            error.append(field)
    if not success:
        # raise Exception("MissingFieldsError", error)
        response = {
            "success" : False,
            "message" : "Below parameters is missing",
            "misssing" : error,
            "required" : required
        }
        return Response(response)
    else:
        return data

def paginate(data, request, type = None, order = True):
    page = request.data.get('page')
    data = data.distinct()
    if order:
        order=type
    if not type:
        reverse=False
    else:
        reverse=orderByMapping[type].get('reverse')
    if page is None:
        page = request.query_params.get('page')
    if order:
        data = data.order_by(order)
    if reverse:
        data = data.reverse()
    if page is None:
        return data[0:20]
    else:
        return data[20*(int(page)-1):20*int(page)-1]