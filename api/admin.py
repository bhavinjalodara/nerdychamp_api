# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .submodels.Interest import Interest
from .submodels.UserDetails import UserDetails
from .submodels.PostDetails import PostDetails
from .submodels.Activities import Activities
from .submodels.CommentDetails import CommentDetails
from .submodels.FileDetails import FileDetails
from .submodels.Vote import Vote

admin.site.register(Interest)
admin.site.register(UserDetails)
admin.site.register(PostDetails)
admin.site.register(Activities)
admin.site.register(CommentDetails)
admin.site.register(FileDetails)
admin.site.register(Vote)
# Register your models here.
