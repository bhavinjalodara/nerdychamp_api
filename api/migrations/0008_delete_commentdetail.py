# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-09 11:25
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_remove_commentdetail_question_text'),
    ]

    operations = [
        migrations.DeleteModel(
            name='CommentDetail',
        ),
    ]
