# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-04 15:16
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0058_auto_20170904_2042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postdetails',
            name='votes',
            field=models.ManyToManyField(related_name='posts', through='api.Vote', to=settings.AUTH_USER_MODEL),
        ),
    ]
