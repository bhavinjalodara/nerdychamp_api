# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-27 16:35
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0044_auto_20170827_2126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='interestweight',
            name='qoutient',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='postdetails',
            name='votes',
            field=models.ManyToManyField(related_name='posts', through='api.Vote', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='postweight',
            name='qoutient',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='userweight',
            name='qoutient',
            field=models.IntegerField(default=0),
        ),
    ]
