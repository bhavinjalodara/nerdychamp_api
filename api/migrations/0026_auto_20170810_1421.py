# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-10 08:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0025_auto_20170810_1415'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserDetails',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('fname', models.CharField(max_length=50)),
                ('lname', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=50)),
                ('dob', models.CharField(max_length=10)),
                ('sex', models.CharField(max_length=10)),
                ('profilePicURL', models.CharField(max_length=50)),
                ('lastLoginKey', models.CharField(max_length=80)),
                ('isEmailVerified', models.BooleanField()),
                ('isDeleted', models.BooleanField()),
                ('password', models.CharField(max_length=80)),
                ('token', models.CharField(max_length=80)),
                ('loginTokenCreatedTime', models.CharField(max_length=50)),
                ('isInterestConfigured', models.BooleanField()),
                ('createdTime', models.CharField(max_length=50)),
                ('modifiedTime', models.CharField(max_length=50)),
                ('deletedTime', models.CharField(max_length=50)),
                ('status', models.CharField(blank=True, max_length=10, null=True)),
                ('followings', models.ManyToManyField(related_name='followers', to='api.UserDetails')),
                ('interests', models.ManyToManyField(to='api.Interest')),
            ],
            options={
                'db_table': 'userdetails',
                'verbose_name': 'UserDetails',
                'verbose_name_plural': 'UserDetailss',
            },
        ),
        migrations.AlterField(
            model_name='postdetails',
            name='userDetail',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='posts', to='api.UserDetails'),
        ),
    ]
