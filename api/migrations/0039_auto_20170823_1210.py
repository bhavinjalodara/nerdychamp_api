# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-23 06:40
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0038_auto_20170819_1618'),
    ]

    operations = [
        migrations.AddField(
            model_name='postdetails',
            name='votes',
            field=models.ManyToManyField(related_name='posts', through='api.Vote', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='vote',
            name='postId',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.PostDetails'),
        ),
        migrations.AlterField(
            model_name='vote',
            name='userId',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
