# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-27 15:56
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0043_auto_20170827_2122'),
    ]

    operations = [
        migrations.CreateModel(
            name='InterestWeight',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('reminder', models.IntegerField()),
                ('qoutient', models.IntegerField()),
                ('interest', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='weight', to='api.Interest')),
            ],
            options={
                'db_table': 'interestweight',
                'verbose_name': 'interestweight',
                'verbose_name_plural': 'interestweights',
            },
        ),
        migrations.AlterField(
            model_name='postdetails',
            name='votes',
            field=models.ManyToManyField(related_name='posts', through='api.Vote', to=settings.AUTH_USER_MODEL),
        ),
    ]
