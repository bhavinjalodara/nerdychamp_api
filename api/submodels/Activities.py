from django.db import models

class Activities(models.Model):

    id = models.BigAutoField(primary_key=True)

    user = models.ForeignKey('UserDetails', on_delete=models.CASCADE, related_name="activities")

    post = models.ForeignKey('PostDetails', on_delete=models.CASCADE, related_name="activities", null=True)

    targetUser = models.ForeignKey('UserDetails', on_delete=models.CASCADE, related_name="targetActivities", null=True)

    action = models.CharField(max_length=10)

    createdTime = models.CharField(max_length=50)

    class Meta:
        """Meta definition for activity."""
        db_table = 'activities'
        verbose_name = 'activity'
        verbose_name_plural = 'activities'

    def __unicode__(self):
        """Unicode representation of activity."""
        return unicode(self.id)