from django.db import models
class FileDetails(models.Model):
    """Model definition for filedetails."""

    # TODO: Define fields here
    id = models.BigAutoField(primary_key=True)

    user = models.ForeignKey('UserDetails', related_name='userFiles')

    fileURL = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        """Meta definition for filedetails."""
        app_label = "api"
        db_table = 'filedetails'
        verbose_name = 'filedetails'
        verbose_name_plural = 'filedetailss'

    def __unicode__(self):
        """Unicode representation of filedetails."""
        return unicode(self.id)
