from django.db import models

class UserWeight(models.Model):

    id = models.BigAutoField(primary_key=True)

    user = models.OneToOneField('UserDetails', on_delete=models.CASCADE, related_name="weight")

    reminder = models.IntegerField()

    qoutient = models.IntegerField(default=0)

    class Meta:
        """Meta definition for userweight."""
        db_table = 'userweight'
        verbose_name = 'userweight'
        verbose_name_plural = 'userweights'

    def __unicode__(self):
        """Unicode representation of userweight."""
        return unicode(self.id)