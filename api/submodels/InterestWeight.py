from django.db import models

class InterestWeight(models.Model):

    id = models.BigAutoField(primary_key=True)

    interest = models.OneToOneField('Interest', on_delete=models.CASCADE, related_name="weight")

    reminder = models.IntegerField()

    qoutient = models.IntegerField(default=0)

    class Meta:
        """Meta definition for interestweight."""
        db_table = 'interestweight'
        verbose_name = 'interestweight'
        verbose_name_plural = 'interestweights'

    def __unicode__(self):
        """Unicode representation of interestweight."""
        return unicode(self.id)