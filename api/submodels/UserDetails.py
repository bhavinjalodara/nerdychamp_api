from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from api.managers.UserManager import UserManager


class UserDetails(AbstractBaseUser,PermissionsMixin):
    """Model definition for UserDetails."""

    #define custom usermanager
    objects=UserManager()

    #define username_field
    USERNAME_FIELD = 'email'

    # TODO: Define fields here
    id = models.BigAutoField(primary_key=True)

    fname = models.CharField(max_length=50)

    lname = models.CharField(max_length=50)

    email = models.CharField(max_length=50,unique=True)

    dob = models.CharField(max_length=10)

    sex = models.CharField(max_length=10)

    profilePicURL = models.CharField(max_length=50, null=True)

    lastLoginKey = models.CharField(max_length=80)

    isEmailVerified = models.NullBooleanField()

    isDeleted = models.NullBooleanField()

    password = models.CharField(max_length=80)

    token = models.CharField(max_length=80)

    loginTokenCreatedTime = models.CharField(max_length=50)

    isInterestConfigured = models.NullBooleanField()

    interests = models.ManyToManyField('Interest', symmetrical=False)

    followings = models.ManyToManyField('self', symmetrical=False, related_name='followers')

    createdTime = models.CharField(max_length=50)

    modifiedTime = models.CharField(max_length=50)

    deletedTime = models.CharField(max_length=50)

    status = models.CharField(max_length=10, blank=True, null=True)

    @property
    def is_staff(self):
        return self.is_superuser

    @property
    def username(self):
        return self.email

    def get_full_name(self):
        return self.fname+" "+self.lname

    def get_short_name(self):
        return self.fname

    def follow(self, person):
        data = self.followings.add(person)
        return data

    def unfollow(self, person):
        self.followings.remove(person)

    def getFollowings(self):
        return self.followings.all()

    def getFollowers(self):
        return self.followers.all()

    class Meta:
        """Meta definition for UserDetails."""
        db_table = 'userdetails'
        verbose_name = 'UserDetails'
        verbose_name_plural = 'UserDetails'

    def __unicode__(self):
        """Unicode representation of UserDetails."""
        return unicode(self.id)
