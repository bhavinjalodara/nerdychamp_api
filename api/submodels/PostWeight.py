from django.db import models

class PostWeight(models.Model):

    id = models.BigAutoField(primary_key=True)

    post = models.OneToOneField('PostDetails', on_delete=models.CASCADE, related_name="weight")

    reminder = models.IntegerField()

    qoutient = models.IntegerField(default=0)

    class Meta:
        """Meta definition for postweight."""
        db_table = 'postweight'
        verbose_name = 'postweight'
        verbose_name_plural = 'postweights'

    def __unicode__(self):
        """Unicode representation of postweight."""
        return unicode(self.id)