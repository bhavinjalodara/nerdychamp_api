from django.db import models
from api.enum.CommentStatus import CommentStatus


class CommentDetails(models.Model):
    """Model definition for CommentDetail."""

    # TODO: Define fields here
    id = models.BigAutoField(primary_key=True)

    # profileId = models.BigIntegerField()

    post = models.ForeignKey('PostDetails', related_name='postComments')

    user = models.ForeignKey('UserDetails', related_name='userComments')

    commentText = models.TextField()

    status = models.CharField(max_length=3,default=CommentStatus.APPROVED)

    createdTime = models.CharField(max_length=50)

    modifiedTime = models.CharField(max_length=50)

    deletedTime = models.CharField(max_length=50)

    agent = models.CharField(max_length=80)

    ipAddress = models.CharField(max_length=50)


    class Meta:
        """Meta definition for CommentDetail."""
        db_table = 'commentdetails'
        verbose_name = 'CommentDetail'
        verbose_name_plural = 'CommentDetails'
    
    def __unicode__(self):
        """Unicode representation of postdetails."""
        return unicode(self.id)