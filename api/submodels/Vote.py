from django.db import models

from api.enum.VoteType import *


class Vote(models.Model):
    """Model definition for vote."""

    # TODO: Define fields here
    id = models.BigAutoField(primary_key=True)

    post = models.ForeignKey('PostDetails', on_delete=models.CASCADE)

    user = models.ForeignKey('UserDetails', on_delete=models.CASCADE)

    type = models.CharField(max_length=2, default=VoteType.DEFAULT)

    createdTime = models.CharField(max_length=50)

    modifiedTime = models.CharField(max_length=50)

    class Meta:
        """Meta definition for vote."""
        db_table = 'vote'
        verbose_name = 'vote'
        verbose_name_plural = 'votes'

    def __unicode__(self):
        """Unicode representation of vote."""
        return unicode(self.id)
