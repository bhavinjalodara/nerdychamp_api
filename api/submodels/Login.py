from django.db import models


class Login(models.Model):
    """Model definition for Login."""

    # TODO: Define fields here
    id = models.BigAutoField(primary_key=True)

    profileId = models.BigIntegerField()

    LoginKey = models.CharField(max_length=80)

    createdTime = models.CharField(max_length=50)

    status = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        """Meta definition for Login."""
        db_table = 'login'
        verbose_name = 'Login'
        verbose_name_plural = 'Logins'

    def __unicode__(self):
        """Unicode representation of Login."""
        pass
