from django.db import models

from api.enum.PostStatus import *


class PostDetails(models.Model):
    """Model definition for postdetails."""

    # TODO: Define fields here
    id = models.BigAutoField(primary_key=True)

    # profileId = models.BigIntegerField()

    postText = models.TextField()

    fileURL = models.CharField(max_length=50, blank=True, null=True)

    status = models.CharField(max_length=3)

    interests = models.ManyToManyField('Interest', related_name='interestPosts')

    user = models.ForeignKey('UserDetails', related_name='userPosts')

    votes = models.ManyToManyField('UserDetails', related_name='posts', through='vote')

    isUrlLink = models.BooleanField(default=False)

    urlTitle = models.CharField(max_length=191, blank=True, null=True)

    urlDescription = models.TextField(default=None, null=True)

    urlImage = models.TextField(default=None, null=True)

    urlLink = models.CharField(max_length=200, blank=True, null=True)

    createdTime = models.CharField(max_length=50)

    modifiedTime = models.CharField(max_length=50)

    deletedTime = models.CharField(max_length=50)

    agent = models.CharField(max_length=50)

    ipAddress = models.CharField(max_length=50)

    class Meta:
        """Meta definition for postdetails."""
        app_label = "api"
        db_table = 'postdetails'
        verbose_name = 'postdetails'
        verbose_name_plural = 'postdetailss'

    def __unicode__(self):
        """Unicode representation of postdetails."""
        return unicode(self.id)
