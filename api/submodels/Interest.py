from django.db import models


class Interest(models.Model):
    """Model definition for Interest."""

    # TODO: Define fields here
    id = models.BigAutoField(primary_key=True)

    interestName = models.CharField(max_length=50)

    status = models.CharField(max_length=10)

    interestImageName = models.CharField(max_length=50, blank=True, unique=True, null=True)

    parentInterestId = models.BigIntegerField(default=0)

    class Meta:
        """Meta definition for Interest."""
        app_label = "api"
        db_table = 'interest'
        verbose_name = 'Interest'
        verbose_name_plural = 'Interests'

    def __unicode__(self):
        """Unicode representation of Interest."""
        return unicode(self.id)
