from collections import namedtuple

__postweight = namedtuple('WeightType', ['CREATE', 'UPVOTE', 'DOWNVOTE', 'DEFAULT', 'LIMIT'])
PostValue = __postweight(0, 1, -1, -1, 2147483647)

__userweight = namedtuple('WeightType', ['CREATE', 'FOLLOW', 'UNFOLLOW', 'LIMIT'])
UserValue = __userweight(0, 1, -1, 2147483647)