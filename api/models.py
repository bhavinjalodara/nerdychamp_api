# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from api.submodels import CommentDetails
from api.submodels.Interest import Interest
from api.submodels import Login
from api.submodels import PostDetails
from api.submodels import UserDetails
from api.submodels import Vote
from api.submodels import FileDetails
from api.submodels import UserWeight
from api.submodels import PostWeight
from api.submodels import InterestWeight
from api.submodels import Activities

# from django.db import models
# Create your models here.
