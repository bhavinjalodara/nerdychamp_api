from collections import namedtuple


__postStatus = namedtuple('PostStatus', ['DRAFT', 'APPROVED', 'BLOCKED', 'REJECTED', 'DELETED'])
PostStatus = __postStatus(0, 1, 2, 3, 4)
