from collections import namedtuple


__activityType = namedtuple('ActivityType', ['FOLLOW', 'UPVOTE', 'COMMENT', 'POST', ])
ActivityType = __activityType('follow', 'upvote', 'comment', 'post')
