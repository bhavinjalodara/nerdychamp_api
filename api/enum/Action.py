from collections import namedtuple


__Action = namedtuple('Action', ['NEW', 'UPDATE', 'DELETE', 'DRAFT', 'UPDATEDRAFT'])
Action = __Action(0, 1, 2, 3, 4)
