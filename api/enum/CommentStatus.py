from collections import namedtuple


__commentStatus = namedtuple('CommentStatus', ['DRAFT', 'APPROVED', 'BLOCKED', 'REJECTED', 'DELETED'])
CommentStatus = __commentStatus(0, 1, 2, 3, 4)
