from collections import namedtuple


__userStatus = namedtuple('UserStatus', ['SUSPEND', 'ACTIVE', 'INACTIVE'])
UserStatus = __userStatus(0, 1, 2)
