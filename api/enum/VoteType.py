from collections import namedtuple


__voteType = namedtuple('VoteType', ['DOWNVOTE', 'UPVOTE', 'DEFAULT'])
VoteType = __voteType(0, 1, 2)
