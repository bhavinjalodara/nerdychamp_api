from rest_framework import serializers

from api.submodels.Interest import Interest


class InterestSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Interest
        fields = ('id', 'interestName', 'status', 'interestImageName', 'parentInterestId')
