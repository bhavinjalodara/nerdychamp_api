from api.Serializers.FollowSerializer import FollowSerializer
from rest_framework import serializers

from api.submodels.CommentDetails import CommentDetails


class CommentSerializer(serializers.HyperlinkedModelSerializer):

    user = FollowSerializer( read_only=True)
    class Meta:
        model = CommentDetails
        fields = ('id', 'commentText','user')