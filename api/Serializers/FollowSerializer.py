from rest_framework import serializers

from api.submodels.UserDetails import UserDetails


class FollowSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserDetails
        fields = ('id', 'fname', 'lname', 'sex', 'profilePicURL')
