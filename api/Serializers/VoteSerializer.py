from rest_framework import serializers

from api.submodels.Vote import Vote


class VoteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Vote
        fields = ('id', 'post_id', 'user_id', 'type')
