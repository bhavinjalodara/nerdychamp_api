from api.Serializers.InterestSerializer import InterestSerializer

from api.Serializers.FollowSerializer import FollowSerializer
from rest_framework import serializers

from api.submodels.UserDetails import UserDetails


class UserSerializer(serializers.HyperlinkedModelSerializer):
    interests=InterestSerializer(many=True,read_only=True)
    followings=FollowSerializer(many=True,read_only=True)
    class Meta:
        model = UserDetails
        fields = ('id', 'fname', 'lname', 'email', 'dob', 'sex', 'profilePicURL', 'isEmailVerified', 'interests','followings', 'isInterestConfigured')
