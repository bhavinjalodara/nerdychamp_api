from api.submodels.Activities import Activities

from api.submodels.PostDetails import PostDetails

from api.Serializers.FollowSerializer import FollowSerializer
from rest_framework import serializers

class AbstractPostSerializer(serializers.HyperlinkedModelSerializer):

    user = FollowSerializer(read_only=True)
    class Meta:
        model = PostDetails
        fields = ( 'id', 'user')

class ActivitySerializer(serializers.HyperlinkedModelSerializer):

    post = AbstractPostSerializer(read_only=True)
    user = FollowSerializer(read_only=True)
    targetUser = FollowSerializer(read_only=True)
    class Meta:
        model = Activities
        fields = ( 'id', 'user', 'post', 'targetUser', 'action')