
from api.Serializers.FollowSerializer import FollowSerializer
from rest_framework import serializers

from api.submodels.FileDetails import FileDetails


class FileSerializer(serializers.HyperlinkedModelSerializer):

    user = FollowSerializer( read_only=True)
    class Meta:
        model = FileDetails
        fields = ('id', 'commentText','user')