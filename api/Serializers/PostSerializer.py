from api.Serializers.InterestSerializer import InterestSerializer

from api.Serializers.FollowSerializer import FollowSerializer
from rest_framework import serializers

from api.submodels.PostDetails import PostDetails


class PostSerializer(serializers.HyperlinkedModelSerializer):

    interests = InterestSerializer(many=True, read_only=True)
    user = FollowSerializer( read_only=True)
    class Meta:
        model = PostDetails
        fields = ('id', 'postText', 'fileURL', 'interests', 'user', 'isUrlLink', 'urlTitle', 'urlDescription', 'urlImage', 'urlLink', 'createdTime')