"""nerdychamp_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from api import views
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'interest', views.InterestViewSet, base_name='interest')
router.register(r'profile', views.ProfileViewSet, base_name='profile')
router.register(r'user', views.UserViewSet, base_name='user')
router.register(r'post', views.VoteViewSet, base_name='vote')
router.register(r'post', views.PostViewSet, base_name='post')
router.register(r'post', views.CommentViewSet, base_name='comment')
router.register(r'file', views.FileViewSet, base_name='file')
router.register(r'', views.InceptViewSet, base_name='incept')

urlpatterns = [
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
]
